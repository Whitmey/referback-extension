// Read and write to DOM, trigger messages to background

function getDataFromDOM(selector) {
  var domNode = document.querySelector(selector);
  if (domNode) {
    var content = document.querySelector(selector).textContent;
    if (content) {
      return content.trim();
    }
  }
  return undefined;
}

// This will emit a message, which could be listened to in component
window.chrome.runtime.sendMessage({
    type: 'OYO_PLUGIN_EVALUATED_CONFIG',
    configData: getDataFromDOM('#someSelector')
});
