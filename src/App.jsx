import React, { Component } from "react";
import "./styles/app.scss";

export class App extends Component{

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div id="referback-extension">
        <h1>Hello there</h1>
      </div>
    );
  }

}

export default App;
